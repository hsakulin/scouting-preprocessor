# 40 MHz scouting preprocessor

This board receives trigger data via 4-8 optical links @ 10 Gb/s and provides their data to an attached CPU-based compute system.

## Requirements

The `ipbb` build environment requires Python 2.7, pip, and virtualenv to run. For the firmware build itself Xilinx Vivado (tested with 2018.2) and for the simulation Mentor Modelsim 10.6a are used.

## Build instructions

A bitfile can be generated in the following way:

```
./scripts/makeProject.sh
./scripts/buildFirmware.sh
```

It can then be found in the folder `build/scouting/proj/scouting_build/package`.

*Note:* If the build scripts find the file `buildToolSetup.sh` in the base directory they will source it. It can be used to set the environment variables required for Vivado and Modelsim. A template (`buildToolSetup_template.sh`) is provided.

## Things to do

There are multiple things that need to be done still, among them
 * receiver logic
  * preferably auto aligning
 * rudimentary zero-supression logic
 * (possibly:) IPbus access
