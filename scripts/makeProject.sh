#!/bin/bash
set -e # Exit on error.
if [ -f buildToolSetup.sh ] ; then
    source buildToolSetup.sh
fi
if [ `command -v vsim` ] ; then
    echo "Found Mentor Modelsim at" `command -v vsim`
else
    echo "Couldn't find Mentor Modelsim. Exiting."
    exit 1
fi
if [ -z ${XILINX_VIVADO:+x} ] ; then
    echo "Xilinx Vivado environment has not been sourced. Exiting."
    exit 1
else
    echo "Found Xilinx Vivado at" ${XILINX_VIVADO}
fi
BASE_DIR=`pwd`

mkdir $BUILD_DIR
cd $BUILD_DIR
# Anonymous checkout of ipbb
#curl -L https://github.com/ipbus/ipbb/archive/v0.3.2.tar.gz | tar xvz
# mv ipbb-0.3.2 ipbb
curl -L https://github.com/dinyar/ipbb/archive/master.tar.gz | tar xvz
mv ipbb-master ipbb
source ipbb/env.sh
ipbb init scouting
mkdir scouting/src/scouting-preprocessor
pushd scouting/src/scouting-preprocessor
ln -sf ../../../../scouting-board
popd
pushd scouting
ipbb proj create vivado scouting_build scouting-preprocessor:scouting-board -t top.dep
popd
pushd scouting/proj/scouting_build/
ipbb vivado project reset
# TODO: Check if this is still necessary
# Link in timing checker script.
# if [ ! -f "checkTiming.py" ]; then
#   ln -s ../../../../scripts/checkTiming.py
# fi
popd
# pushd scouting/proj/scouting_sim/
# ipbb sim ipcores fli
# ipbb sim project
