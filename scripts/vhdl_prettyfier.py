#!/usr/local/env python3

import subprocess
from pathlib import Path
import click

@click.command()
@click.argument('path')
def main(path):
    d = Path(path)
    for p in d.glob('**/*.vhd'):
        tmp_file = Path("tmp.vhd")
        with p.open() as f:
            print(p)
            with tmp_file.open('w') as t:
                subprocess.check_call(['./vhdl-pretty'], stdin=f, stdout=t)
        tmp_file.replace(p)

if __name__ == "__main__":
    main()
