-- top_decl
--
-- Constants for the whole device
--
-- D. R. May 2018

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package top_decl is

    constant ALGO_REV        : std_logic_vector(31 downto 0) := X"00" & X"00" & X"00" & X"01";
    constant LHC_BUNCH_COUNT : integer                       := 3564;
    constant N_REGION        : positive                      := 2;

    constant ORBIT_LENGTH : natural := 3564;

end top_decl;
