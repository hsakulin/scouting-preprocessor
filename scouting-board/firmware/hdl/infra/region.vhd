-- region
--
-- Collection of four input links
--
-- D. R. May 2018
library IEEE;
use IEEE.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

use work.datatypes.all;

entity region is
    port (
        clk           : in  std_logic;
        rst           : in  std_logic;
        align_marker  : in  std_logic;
        q             : out ldata(3 downto 0);
        status_ok     : out std_logic;
        status_locked : out std_logic
        );
end region;

architecture Behavioral of region is
begin

    gth_wrapper : entity work.gth_wrapper_10g
        port map(
            clk           => clk,
            rst           => rst,
            q             => q,
            status_ok     => status_ok,
            status_locked => status_locked
            );

-- TODO: Include alginment module here.

end Behavioral;
