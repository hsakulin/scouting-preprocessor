--
-- Top-level entity for KCU1500 dev kit
--
-- D. R. May 2018
library ieee;
use ieee.std_logic_1164.all;

use IEEE.NUMERIC_STD.all;

library unisim;
use unisim.vcomponents.all;

use work.top_decl.all;
use work.datatypes.all;

entity top is
    port (
        -- MGT ref clock
        mgtrefclk0_x0y3_p : in std_logic;
        mgtrefclk0_x0y3_n : in std_logic;

        -- Serial data ports for transceiver channel 0
        ch0_gthrxn_in  : in  std_logic;
        ch0_gthrxp_in  : in  std_logic;
        ch0_gthtxn_out : out std_logic;
        ch0_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 1
        ch1_gthrxn_in  : in  std_logic;
        ch1_gthrxp_in  : in  std_logic;
        ch1_gthtxn_out : out std_logic;
        ch1_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 2
        ch2_gthrxn_in  : in  std_logic;
        ch2_gthrxp_in  : in  std_logic;
        ch2_gthtxn_out : out std_logic;
        ch2_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 3
        ch3_gthrxn_in  : in  std_logic;
        ch3_gthrxp_in  : in  std_logic;
        ch3_gthtxn_out : out std_logic;
        ch3_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 4
        ch4_gthrxn_in  : in  std_logic;
        ch4_gthrxp_in  : in  std_logic;
        ch4_gthtxn_out : out std_logic;
        ch4_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 5
        ch5_gthrxn_in  : in  std_logic;
        ch5_gthrxp_in  : in  std_logic;
        ch5_gthtxn_out : out std_logic;
        ch5_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 6
        ch6_gthrxn_in  : in  std_logic;
        ch6_gthrxp_in  : in  std_logic;
        ch6_gthtxn_out : out std_logic;
        ch6_gthtxp_out : out std_logic;

        -- Serial data ports for transceiver channel 7
        ch7_gthrxn_in  : in  std_logic;
        ch7_gthrxp_in  : in  std_logic;
        ch7_gthtxn_out : out std_logic;
        ch7_gthtxp_out : out std_logic;

        -- User-provided ports for reset helper block(s)
        sysclk300_p : in std_logic;
        sysclk300_n : in std_logic;
--        hb_gtwiz_reset_all_in : in std_logic;

        -- Clock for PCIe
        sysclk_in_p : in std_logic;
        sysclk_in_n : in std_logic;
        sys_rst_n   : in std_logic;

        pci_exp_rxp : in  std_logic_vector(7 downto 0);
        pci_exp_rxn : in  std_logic_vector(7 downto 0);
        pci_exp_txp : out std_logic_vector(7 downto 0);
        pci_exp_txn : out std_logic_vector(7 downto 0);

        sda                 : inout std_logic;
        scl                 : inout std_logic;
        I2C_MAIN_RESET_B_LS : out   std_logic
        );
end top;

architecture Behavioral of top is

    signal mgtrefclk0_x0y3_int : std_logic;
    signal sysclk300_in        : std_logic;


    signal d_gap_cleaner, d_align, d_algo_legacy : ldata(3 + (N_REGION - 1) * 4 downto 0);
    signal d_align_reg                           : ldata(3 + (N_REGION - 1) * 4 downto 0);

    signal d_algo, d_zs, q_zs, d_package                     : adata(4*N_REGION -1 downto 0);
    signal d_algo_reg                                        : adata(4*N_REGION -1 downto 0);
    signal d_ctrl_algo, d_ctrl_zs, q_ctrl_zs, d_ctrl_package : acontrol;
    signal d_ctrl_algo_reg                                   : acontrol;

    signal rst_global              : std_logic;
    signal clk_algo, rst_algo      : std_logic;
    signal rst_packager  : std_logic;
    signal rst_aligner       : std_logic;

    signal axi_clk, axi_clk4, axi_rstn, axi_rst                  : std_logic;
    signal pcie_lnk_up                                           : std_logic                     := '0';
    signal m_axis_c2h_tdata_0, m_axis_h2c_tdata_0, test_tdata    : std_logic_vector(255 downto 0);  -- Not sure about test_tdata
    signal m_axis_c2h_tlast_0, m_axis_h2c_tlast_0, test_tlast    : std_logic;
    signal m_axis_c2h_tvalid_0, m_axis_h2c_tvalid_0, test_tvalid : std_logic;
    signal m_axis_c2h_tready_0, m_axis_h2c_tready_0, test_tready : std_logic;
    signal m_axis_c2h_tkeep_0, m_axis_h2c_tkeep_0, test_tkeep    : std_logic_vector(31 downto 0);
    signal packet_length                                         : std_logic_vector(15 downto 0) := x"0020";
    signal fifo_cnt                                              : std_logic_vector(14 downto 0) := (others => '0');
    signal fifo_read, fifo_write, fifo_empty                     : std_logic                     := '0';
    type state_s is (idle, wait_s1, wait_s2, wait_s3, wait_s4, read);
    signal state                                                 : state_s                       := idle;

    signal validVec, doneVec : std_logic_vector(7 downto 0);

--    signal link_status_out                            : std_logic_vector(0 downto 0);
--    signal link_down_latched_out                      : std_logic_vector(0 downto 0);
    signal init_done_int                              : std_logic_vector(0 downto 0);
    signal init_retry_ctr_int                         : std_logic_vector(3 downto 0);
    signal gtpowergood_vio_sync                       : std_logic_vector(4*N_REGION - 1 downto 0);
    signal txpmaresetdone_vio_sync                    : std_logic_vector(4*N_REGION - 1 downto 0);
    signal rxpmaresetdone_vio_sync                    : std_logic_vector(4*N_REGION - 1 downto 0);
    signal gtwiz_reset_tx_done_vio_sync               : std_logic_vector(0 downto 0);
    signal gtwiz_reset_rx_done_vio_sync               : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_all_vio_int                 : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_pll_and_datapath_int    : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_datapath_int            : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_pll_and_datapath_vio_int : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_datapath_vio_int         : std_logic_vector(0 downto 0);
    signal link_down_latched_reset_vio_int            : std_logic_vector(0 downto 0);
    signal loopback                                   : std_logic_vector(3*4*N_REGION - 1 downto 0);

    signal rx_data     : std_logic_vector(8 * 32 - 1 downto 0);
    signal trigger_ila : std_logic;

    signal ocounter         : std_logic_vector(31 downto 0);
    signal bcounter         : std_logic_vector(31 downto 0);
    signal evt_word         : std_logic_vector(31 downto 0);
    signal sRxbyteisaligned : std_logic_vector(4*N_REGION - 1 downto 0);
    signal cdr_stable       : std_logic_vector(0 downto 0);

    signal clk_i2c, hb_gtwiz_reset_clk_freerun_in, hb_gtwiz_reset_clk_freerun_int, clk_fb : std_logic;

    signal str_rd, str_wr, rst_i2c : std_logic;
    signal data_rd                 : std_logic_vector(7 downto 0);
    signal sCommaDet               : std_logic_vector(4*N_REGION - 1 downto 0);

    signal stream_enable_mask     : std_logic_vector(4*N_REGION - 1 downto 0);
    signal stream_enable_mask_vio : std_logic_vector(7 downto 0);

    signal axi_prog_full : std_logic;

    signal filler_orbit_counter         : unsigned(64-1 downto 0);
    signal filler_dropped_orbit_counter : unsigned(64-1 downto 0);
    signal axi_backpressure_seen        : std_logic;
    signal orbits_per_packet            : std_logic_vector(15 downto 0);
    signal waiting_orbit_end            : std_logic;

    signal do_zs : std_logic;

    signal orbit_length        : unsigned (63 downto 0);
    signal orbit_exceeds_size  : std_logic;
    signal enable_autorealign  : std_logic;
    signal autorealign_counter : unsigned (63 downto 0);

    signal freq_input : std_logic_vector(31 downto 0);

    signal rxctrl0_int : std_logic_vector(63 downto 0);
    signal rxctrl1_int : std_logic_vector(63 downto 0);
    signal rxctrl2_int : std_logic_vector(31 downto 0);
    signal rxctrl3_int : std_logic_vector(31 downto 0);

    component fifo_generator_0 is
        port (
            s_aresetn     : in  std_logic                        := '0';
            m_axis_tvalid : out std_logic                        := '0';
            m_axis_tready : in  std_logic                        := '0';
            m_axis_tdata  : out std_logic_vector(256-1 downto 0) := (others => '0');
            m_axis_tkeep  : out std_logic_vector(32-1 downto 0)  := (others => '0');
            m_axis_tlast  : out std_logic                        := '0';
            s_axis_tvalid : in  std_logic                        := '0';
            s_axis_tready : out std_logic                        := '0';
            s_axis_tdata  : in  std_logic_vector(256-1 downto 0) := (others => '0');
            s_axis_tkeep  : in  std_logic_vector(32-1 downto 0)  := (others => '0');
            s_axis_tlast  : in  std_logic                        := '0';
            axi_prog_full : out std_logic                        := '0';
            m_aclk        : in  std_logic                        := '0';
            s_aclk        : in  std_logic                        := '0');
    end component;

    component link_vio
        port (
            clk         : in  std_logic;
            probe_in0   : in  std_logic_vector(0 downto 0);
            probe_in1   : in  std_logic_vector(0 downto 0);
            probe_in2   : in  std_logic_vector(0 downto 0);
            probe_in3   : in  std_logic_vector(3 downto 0);
            probe_in4   : in  std_logic_vector(7 downto 0);
            probe_in5   : in  std_logic_vector(7 downto 0);
            probe_in6   : in  std_logic_vector(7 downto 0);
            probe_in7   : in  std_logic_vector(0 downto 0);
            probe_in8   : in  std_logic_vector(0 downto 0);
            probe_in9   : in  std_logic_vector(7 downto 0);
            probe_in10  : in  std_logic_vector(0 downto 0);
            probe_in11  : in  std_logic_vector(7 downto 0);
            probe_in12  : in  std_logic_vector(14 downto 0);
            probe_in13  : in  std_logic_vector(14 downto 0);
            probe_in14  : in  std_logic_vector(14 downto 0);
            probe_in15  : in  std_logic_vector(14 downto 0);
            probe_in16  : in  std_logic_vector(14 downto 0);
            probe_in17  : in  std_logic_vector(14 downto 0);
            probe_in18  : in  std_logic_vector(14 downto 0);
            probe_in19  : in  std_logic_vector(14 downto 0);
            probe_in20  : in  std_logic_vector(14 downto 0);
            probe_in21  : in  std_logic_vector(14 downto 0);
            probe_in22  : in  std_logic_vector(14 downto 0);
            probe_in23  : in  std_logic_vector(14 downto 0);
            probe_in24  : in  std_logic_vector(14 downto 0);
            probe_in25  : in  std_logic_vector(14 downto 0);
            probe_in26  : in  std_logic_vector(14 downto 0);
            probe_in27  : in  std_logic_vector(14 downto 0);
            probe_in28  : in  std_logic_vector(63 downto 0);
            probe_in29  : in  std_logic_vector(63 downto 0);
            probe_in30  : in  std_logic_vector(2 downto 0);
            probe_in31  : in  std_logic_vector(9 downto 0);
            probe_in32  : in  std_logic_vector(63 downto 0);
            probe_in33  : in  std_logic_vector(31 downto 0);
            probe_in34  : in  std_logic_vector(63 downto 0);
            probe_out0  : out std_logic_vector(0 downto 0);
            probe_out1  : out std_logic_vector(0 downto 0);
            probe_out2  : out std_logic_vector(7 downto 0);
            probe_out3  : out std_logic_vector(0 downto 0);
            probe_out4  : out std_logic_vector(0 downto 0);
            probe_out5  : out std_logic_vector(0 downto 0);
            probe_out6  : out std_logic_vector(23 downto 0);
            probe_out7  : out std_logic_vector(0 downto 0);
            probe_out8  : out std_logic_vector(0 downto 0);
            probe_out9  : out std_logic_vector(0 downto 0);
            probe_out10 : out std_logic_vector(0 downto 0);
            probe_out11 : out std_logic_vector(15 downto 0);
            probe_out12 : out std_logic_vector(0 downto 0);
            probe_out13 : out std_logic_vector(0 downto 0)
            );
    end component;

    component axi_ila_0
        
        port (
            clk         : in  std_logic;
            trig_in     : in  std_logic;
            trig_in_ack : out std_logic;
            probe0      : in  std_logic_vector(0 downto 0);
            probe1      : in  std_logic_vector(511 downto 0);
            probe2      : in  std_logic_vector(63 downto 0);
            probe3      : in  std_logic_vector(0 downto 0);
            probe4      : in  std_logic_vector(0 downto 0);
            probe5      : in  std_logic_vector(63 downto 0);
            probe6      : in  std_logic_vector(63 downto 0);
            probe7      : in  std_logic_vector(31 downto 0);
            probe8      : in  std_logic_vector(31 downto 0)
            );
    end component;

    -- DEBUG
    signal enable_i2c_gen, rst_i2c_gen, wr_i2c_gen : std_logic;
begin

    global_reset : entity work.reset
        port map (
            clk_free     => hb_gtwiz_reset_clk_freerun_in,
            clk_i2c      => clk_i2c,
            clk_algo     => clk_algo,
            clk_axi      => axi_clk,
            rst_global   => rst_global,
            enable_i2c   => enable_i2c_gen,
--      enable_i2c   => I2C_MAIN_RESET_B_LS,
            rst_i2c      => rst_i2c_gen,
--      rst_i2c      => rst_i2c,
            write_i2c    => wr_i2c_gen,
--      write_i2c    => str_wr,
            rst_pll      => hb0_gtwiz_reset_tx_pll_and_datapath_int(0),
            rst_tx       => hb0_gtwiz_reset_tx_datapath_int(0),
            rst_rx       => hb_gtwiz_reset_rx_datapath_vio_int(0),
            rst_algo     => rst_algo,
            rst_packager => rst_packager
            );

    -- Set MGT clock via I2C 
    ---------------------------------------------------------------------------
    i2c_i : entity work.i2c_driver
        port map (
            clk     => clk_i2c,         -- 50 MHz from PLL below.
            reset   => rst_i2c,
            str_wr  => str_wr,
            str_rd  => str_rd,
            data_rd => data_rd,
            sda     => sda,
            scl     => scl
            );

    I2C_MAIN_RESET_B_LS <= '1';         -- TODO: Set via VIO?
    ---------------------------------------------------------------------------


    -- Differential reference clock buffer for freerunning clock
    IBUFDS_FREERUN_INST : IBUFDS
        port map (
            I  => sysclk300_p,
            IB => sysclk300_n,
            O  => sysclk300_in
            );

    PLLE3_ADV_INST : PLLE3_ADV
        generic map (
            COMPENSATION       => "AUTO",
            STARTUP_WAIT       => "FALSE",
            DIVCLK_DIVIDE      => 1,
            CLKFBOUT_MULT      => 2,
            CLKFBOUT_PHASE     => 0.000,
            CLKOUT0_DIVIDE     => 4,
            CLKOUT0_PHASE      => 0.000,
            CLKOUT0_DUTY_CYCLE => 0.500,
            CLKOUT1_DIVIDE     => 12,
            CLKOUT1_PHASE      => 0.000,
            CLKOUT1_DUTY_CYCLE => 0.500,
            CLKIN_PERIOD       => 3.333)
        port map (
            CLKFBOUT    => clk_fb,
            CLKOUT0     => hb_gtwiz_reset_clk_freerun_in,
            CLKOUT0B    => open,
            CLKOUT1     => clk_i2c,
            CLKOUT1B    => open,
            CLKFBIN     => clk_fb,
            CLKIN       => sysclk300_in,
            DADDR       => (others => '0'),
            DCLK        => '0',
            DEN         => '0',
            DI          => (others => '0'),
            DO          => open,
            DRDY        => open,
            DWE         => '0',
            CLKOUTPHYEN => '0',
            PWRDWN      => '0',
            RST         => '0'
            );

--  BUFG_CLOCK_FREE : BUFG
--    port map(
--        I => hb_gtwiz_reset_clk_freerun_in,
--        O => hb_gtwiz_reset_clk_freerun_int
--    );

    -- Differential reference clock buffer for mgtrefclk0
    IBUFDS_GTE3_MGTREFCLK0_X0Y3_INST : IBUFDS_GTE3
        generic map (
            REFCLK_EN_TX_PATH  => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX    => "00")
        port map (
            I     => mgtrefclk0_x0y3_p,
            IB    => mgtrefclk0_x0y3_n,
            CEB   => '0',
            O     => mgtrefclk0_x0y3_int,
            ODIV2 => open
            );

    central_vio : link_vio
        port map (
            clk                                   => axi_clk, -- hb_gtwiz_reset_clk_freerun_int,
            probe_in0(0)                          => '0',
            probe_in1(0)                          => '0',
            probe_in2                             => init_done_int,
            probe_in3                             => init_retry_ctr_int,
            probe_in4(4*N_REGION - 1 downto 0)    => gtpowergood_vio_sync,
            probe_in5(4*N_REGION - 1 downto 0)    => txpmaresetdone_vio_sync,
            probe_in6(4*N_REGION - 1 downto 0)    => rxpmaresetdone_vio_sync,
            probe_in7                             => gtwiz_reset_tx_done_vio_sync,
            probe_in8                             => gtwiz_reset_rx_done_vio_sync,
            probe_in9(4*N_REGION - 1 downto 0)    => sRxbyteisaligned,
            probe_in10                            => cdr_stable,
            probe_in11                            => data_rd,
            probe_in12                            => (others=>'0'),
            probe_in13                            => (others=>'0'),
            probe_in14                            => (others=>'0'),
            probe_in15                            => (others=>'0'),
            probe_in16                            => (others=>'0'),
            probe_in17                            => (others=>'0'),
            probe_in18                            => (others=>'0'),
            probe_in19                            => (others=>'0'),
            probe_in20                            => (others=>'0'),
            probe_in21                            => (others=>'0'),
            probe_in22                            => (others=>'0'),
            probe_in23                            => (others=>'0'),
            probe_in24                            => (others=>'0'),
            probe_in25                            => (others=>'0'),
            probe_in26                            => (others=>'0'),
            probe_in27                            => (others=>'0'),
            probe_in28                            => std_logic_vector(filler_orbit_counter),
            probe_in29                            => std_logic_vector(filler_dropped_orbit_counter),
            probe_in30(0)                         => axi_backpressure_seen,
            probe_in30(1)                         => orbit_exceeds_size,
            probe_in30(2)                         => waiting_orbit_end,
            probe_in31(0)                         => rst_global,
            probe_in31(1)                         => enable_i2c_gen,
            probe_in31(2)                         => rst_i2c_gen,
            probe_in31(3)                         => wr_i2c_gen,
            probe_in31(4)                         => hb0_gtwiz_reset_tx_pll_and_datapath_int(0),
            probe_in31(5)                         => hb0_gtwiz_reset_tx_datapath_int(0),
            probe_in31(6)                         => hb_gtwiz_reset_rx_datapath_vio_int(0),
            probe_in31(7)                         => rst_algo,
            probe_in31(8)                         => rst_packager,
            probe_in31(9)                         => rst_aligner,
            probe_in32                            => std_logic_vector(autorealign_counter),
            probe_in33                            => freq_input,
            probe_in34                            => std_logic_vector(orbit_length),
            probe_out0                            => hb_gtwiz_reset_all_vio_int,
            probe_out1(0)                         => rst_global,
            probe_out2                            => stream_enable_mask_vio,
            probe_out3                            => hb_gtwiz_reset_rx_pll_and_datapath_vio_int,
            probe_out4                            => open,
            probe_out5                            => link_down_latched_reset_vio_int,
            probe_out6(3*4*N_REGION - 1 downto 0) => loopback,
            probe_out7(0)                         => trigger_ila,
            probe_out8(0)                         => rst_i2c,
            probe_out9(0)                         => str_rd,
            probe_out10(0)                        => str_wr,
            probe_out11                           => orbits_per_packet,
            probe_out12(0)                        => do_zs,
            probe_out13(0)                        => enable_autorealign
            );

    stream_enable_mask <= stream_enable_mask_vio(4*N_REGION - 1 downto 0);

    reg_ila_inputs_clk_algo : process (clk_algo) is
    begin
        if (clk_algo'event and clk_algo = '1') then
            d_align_reg     <= d_align;
        end if;
    end process;

    reg_ila_inputs_clk_axi : process (axi_clk) is
    begin
        if (axi_clk'event and axi_clk = '1') then
            d_algo_reg      <= d_algo;
            d_ctrl_algo_reg <= d_ctrl_algo;
        end if;
    end process;


    fifo_axi_ila : axi_ila_0
        port map (
            clk                    => axi_clk,
            trig_in                => '0',
            trig_in_ack            => open,
            ---- TREADY
            probe0                 => (others => '0'),
            ---- TDATA
            probe1(31 downto 0)    => d_align(0).data,
            probe1(63 downto 32)   => d_align(1).data,
            probe1(95 downto 64)   => d_align(2).data,
            probe1(127 downto 96)  => d_align(3).data,
            probe1(159 downto 128) => d_align(4).data,
            probe1(191 downto 160) => d_align(5).data,
            probe1(223 downto 192) => d_align(6).data,
            probe1(255 downto 224) => d_align(7).data,
            probe1(287 downto 256) => d_algo(0),
            probe1(319 downto 288) => d_algo(1),
            probe1(351 downto 320) => d_algo(2),
            probe1(383 downto 352) => d_algo(3),
            probe1(415 downto 384) => d_algo(4),
            probe1(447 downto 416) => d_algo(5),
            probe1(479 downto 448) => d_algo(6),
            probe1(511 downto 480) => d_algo(7),
            ---- TSTRB
            probe2(0)              => d_align(0).valid,
            probe2(1)              => d_align(1).valid,
            probe2(2)              => d_align(2).valid,
            probe2(3)              => d_align(3).valid,
            probe2(4)              => d_align(4).valid,
            probe2(5)              => d_align(5).valid,
            probe2(6)              => d_align(6).valid,
            probe2(7)              => d_align(7).valid,
            probe2(8)              => d_align(0).strobe,
            probe2(9)              => d_align(1).strobe,
            probe2(10)             => d_align(2).strobe,
            probe2(11)             => d_align(3).strobe,
            probe2(12)             => d_align(4).strobe,
            probe2(13)             => d_align(5).strobe,
            probe2(14)             => d_align(6).strobe,
            probe2(15)             => d_align(7).strobe,
            probe2(16)             => d_ctrl_algo.valid,
            probe2(17)             => d_ctrl_algo.strobe,
            probe2(18)             => d_ctrl_algo.bx_start,
            probe2(19)             => d_ctrl_algo.last,
            probe2(20)             => axi_backpressure_seen,
            probe2(21)             => orbit_exceeds_size,
            probe2(63 downto 22)   => (others => '0'),
            ---- TVALID
            probe3(0)              => rst_packager,
            ---- TLAST
            probe4                 => (others => '0'),
            ---- TUSER
            probe5                 => (others => '0'),
            ---- TKEEP
            probe6                 => (others => '0'),
            ---- TDEST
            probe7                 => (others => '0'),
            ---- TID 
            probe8                 => (others => '0')
            );

    inputs : entity work.inputs
        port map(
            axi_clk        => axi_clk,
            mgtrefclk0     => mgtrefclk0_x0y3_int,
--            mgtrefclk0_p  => mgtrefclk0_x0y3_p,
--            mgtrefclk0_n  => mgtrefclk0_x0y3_n,
            -- Serial data ports for transceiver channel 0
            ch0_gthrxn_in  => ch0_gthrxn_in,
            ch0_gthrxp_in  => ch0_gthrxp_in,
            ch0_gthtxn_out => ch0_gthtxn_out,
            ch0_gthtxp_out => ch0_gthtxp_out,

            -- Serial data ports for transceiver channel 1
            ch1_gthrxn_in  => ch1_gthrxn_in,
            ch1_gthrxp_in  => ch1_gthrxp_in,
            ch1_gthtxn_out => ch1_gthtxn_out,
            ch1_gthtxp_out => ch1_gthtxp_out,

            -- Serial data ports for transceiver channel 2
            ch2_gthrxn_in  => ch2_gthrxn_in,
            ch2_gthrxp_in  => ch2_gthrxp_in,
            ch2_gthtxn_out => ch2_gthtxn_out,
            ch2_gthtxp_out => ch2_gthtxp_out,

            -- Serial data ports for transceiver channel 3
            ch3_gthrxn_in  => ch3_gthrxn_in,
            ch3_gthrxp_in  => ch3_gthrxp_in,
            ch3_gthtxn_out => ch3_gthtxn_out,
            ch3_gthtxp_out => ch3_gthtxp_out,

            -- Serial data ports for transceiver channel 4
            ch4_gthrxn_in  => ch4_gthrxn_in,
            ch4_gthrxp_in  => ch4_gthrxp_in,
            ch4_gthtxn_out => ch4_gthtxn_out,
            ch4_gthtxp_out => ch4_gthtxp_out,

            -- Serial data ports for transceiver channel 5
            ch5_gthrxn_in  => ch5_gthrxn_in,
            ch5_gthrxp_in  => ch5_gthrxp_in,
            ch5_gthtxn_out => ch5_gthtxn_out,
            ch5_gthtxp_out => ch5_gthtxp_out,

            -- Serial data ports for transceiver channel 6
            ch6_gthrxn_in  => ch6_gthrxn_in,
            ch6_gthrxp_in  => ch6_gthrxp_in,
            ch6_gthtxn_out => ch6_gthtxn_out,
            ch6_gthtxp_out => ch6_gthtxp_out,

            -- Serial data ports for transceiver channel 7
            ch7_gthrxn_in  => ch7_gthrxn_in,
            ch7_gthrxp_in  => ch7_gthrxp_in,
            ch7_gthtxn_out => ch7_gthtxn_out,
            ch7_gthtxp_out => ch7_gthtxp_out,

            clk_freerun                                => hb_gtwiz_reset_clk_freerun_in,
            clk_freerun_buf                            => hb_gtwiz_reset_clk_freerun_int,  -- Clock after being globally buffered
--            link_status_out     => link_status_out,
--            link_down_latched_out     => link_down_latched_out,
            init_done_int                              => init_done_int,
            init_retry_ctr_int                         => init_retry_ctr_int,
            gtpowergood_vio_sync                       => gtpowergood_vio_sync,
            txpmaresetdone_vio_sync                    => txpmaresetdone_vio_sync,
            rxpmaresetdone_vio_sync                    => rxpmaresetdone_vio_sync,
            gtwiz_reset_tx_done_vio_sync               => gtwiz_reset_tx_done_vio_sync,
            gtwiz_reset_rx_done_vio_sync               => gtwiz_reset_rx_done_vio_sync,
            hb_gtwiz_reset_all_vio_int                 => hb_gtwiz_reset_all_vio_int,
            hb0_gtwiz_reset_tx_pll_and_datapath_int    => hb0_gtwiz_reset_tx_pll_and_datapath_int,
            hb0_gtwiz_reset_tx_datapath_int            => hb0_gtwiz_reset_tx_datapath_int,
            hb_gtwiz_reset_rx_pll_and_datapath_vio_int => hb_gtwiz_reset_rx_pll_and_datapath_vio_int,
            hb_gtwiz_reset_rx_datapath_vio_int         => hb_gtwiz_reset_rx_datapath_vio_int,
            link_down_latched_reset_vio_int            => link_down_latched_reset_vio_int,
            loopback                                   => loopback,

            oLinkData => rx_data,

            oEvt_word        => evt_word,
            oRxbyteisaligned => sRxbyteisaligned,
            cdr_stable       => cdr_stable,
            oBcounter        => bcounter,
            oOcounter        => ocounter,
            iReady           => test_tready,
            rxctrl0_out      => rxctrl0_int,
            rxctrl1_out      => rxctrl1_int,
            rxctrl2_out      => rxctrl2_int,
            rxctrl3_out      => rxctrl3_int,

            clk           => clk_algo,
            q             => d_gap_cleaner,
            oCommaDet     => sCommaDet,
            status_ok     => open,
            status_locked => open
            );

    frequ_meas : entity work.freq_meas
        port map (
            clk => axi_clk,
            rst => rst_packager,
            clk_meas => clk_algo,
            freq => freq_input
            );

    gap_cleaner : entity work.comma_gap_cleaner
        port map (
            clk => clk_algo,
            rst => rst_algo,
            d   => d_gap_cleaner,
            q   => d_align
            );


    auto_realign_controller_1 : entity work.auto_realign_controller
        port map (
            axi_clk           => axi_clk,
            rst_in            => rst_packager,
            enabled           => enable_autorealign,
            clk_aligner       => clk_algo,
            rst_aligner_out   => rst_aligner,
            misalignment_seen => orbit_exceeds_size,
            autoreset_count   => autorealign_counter);

    align : entity work.bx_aware_aligner
        generic map (
            NSTREAMS => 4* N_REGION)
        port map (
            clk_wr => clk_algo,
            rst    => rst_aligner,
            d      => d_align,
            enable => stream_enable_mask,
            waiting_orbit_end => waiting_orbit_end,
            clk_rd => axi_clk,
            q      => d_zs,
            q_ctrl => d_ctrl_zs
            );

--    zs: entity work.zero_suppression
--    port map (
--      clk          => clk_algo,
--      rst          => rst_packager,
--      bunch_marker => d_ctrl_zs.bx_start,
--      d            => d_zs,
--      control_in   => d_ctrl_zs,
--      q            => q_zs,
--      control_out  => q_ctrl_zs);
    
    zs_alt : entity work.zs_alt
        generic map (
            NSTREAMS => 4* N_REGION)
        port map (
            clk    => axi_clk,
            rst    => rst_packager,
            d      => d_zs,
            d_ctrl => d_ctrl_zs,
            q      => q_zs,
            q_ctrl => q_ctrl_zs);


    d_algo      <= q_zs      when do_zs = '1' else d_zs;
    d_ctrl_algo <= q_ctrl_zs when do_zs = '1' else d_ctrl_zs;

    algo : entity work.algo
        port map(
            clk    => axi_clk,
            rst    => rst_packager,
            d      => d_algo,
            d_ctrl => d_ctrl_algo,
            q      => d_package,
            q_ctrl => d_ctrl_package
            );


    fifo_filler : entity work.fifo_filler
        generic map (
            NSTREAMS => 4* N_REGION)
        port map (
            d_clk                  => axi_clk,
            rst                    => rst_packager,
            orbits_per_packet      => unsigned(orbits_per_packet),
            d                      => d_package,
            d_ctrl                 => d_ctrl_package,
            m_aclk                 => axi_clk,
            m_axis_tvalid          => m_axis_c2h_tvalid_0,
            m_axis_tready          => m_axis_c2h_tready_0,
            m_axis_tdata           => m_axis_c2h_tdata_0,
            m_axis_tkeep           => m_axis_c2h_tkeep_0,
            m_axis_tlast           => m_axis_c2h_tlast_0,
            dropped_orbit_counter  => filler_dropped_orbit_counter,
            orbit_counter          => filler_orbit_counter,
            axi_backpressure_seen  => axi_backpressure_seen,
            orbit_length           => orbit_length,   
            orbit_exceeds_size     => orbit_exceeds_size,
            in_autorealign_counter => autorealign_counter);


    dma_engine_i : entity work.xilinx_dma_pcie_ep
        generic map(
            PL_LINK_CAP_MAX_LINK_WIDTH => 8,
            PL_SIM_FAST_LINK_TRAINING  => false,
            PL_LINK_CAP_MAX_LINK_SPEED => 4,
            C_DATA_WIDTH               => 256,
            EXT_PIPE_SIM               => false,
            C_ROOT_PORT                => false,
            C_DEVICE_NUMBER            => 0)
        port map (
            pci_exp_txp         => pci_exp_txp,
            pci_exp_txn         => pci_exp_txn,
            pci_exp_rxp         => pci_exp_rxp,
            pci_exp_rxn         => pci_exp_rxn,
            sys_clk_p           => sysclk_in_p,
            sys_clk_n           => sysclk_in_n,
            sys_rst_n           => sys_rst_n,
            axi_clk             => axi_clk,  -- This is the clock I want to run the algo with?
            axi_rstn            => axi_rstn,
            pcie_lnk_up         => pcie_lnk_up,
--            m_axis_c2h_tdata_0  => test_tdata,
            m_axis_c2h_tdata_0  => m_axis_c2h_tdata_0,
            m_axis_h2c_tdata_0  => m_axis_h2c_tdata_0,
--            m_axis_c2h_tlast_0  => doneVec(0),
            m_axis_c2h_tlast_0  => m_axis_c2h_tlast_0,
            m_axis_h2c_tlast_0  => m_axis_h2c_tlast_0,
--            m_axis_c2h_tvalid_0 => validVec(0),
            m_axis_c2h_tvalid_0 => m_axis_c2h_tvalid_0,
            m_axis_h2c_tvalid_0 => m_axis_h2c_tvalid_0,
            m_axis_c2h_tready_0 => m_axis_c2h_tready_0,
            m_axis_h2c_tready_0 => m_axis_h2c_tready_0,
            m_axis_c2h_tkeep_0  => m_axis_c2h_tkeep_0,
            m_axis_h2c_tkeep_0  => m_axis_h2c_tkeep_0);

end Behavioral;
